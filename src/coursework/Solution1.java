/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import CMPC3M06.AudioPlayer;
import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

/**
 *
 * @author ugg14wmu
 */
public class Solution1 {
    public static void receiver(AudioPlayer player, DatagramSocket socket) {
        byte[] buffer;
        
        while (VoIPSystem.running){
            try {
                // Receive a DatagramPacket with voice data of 512 bits each.
                buffer = new byte[512];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

                socket.receive(packet);

                // The UDP packet contains the voice data. Play it out.
                player.playBlock(packet.getData());

            } catch (SocketTimeoutException e) {
                try {
                    //this.player.playBlock(buffer);
                } catch (Exception ex) {
                    System.out.println("NO PLAY");
                }
                // No packet received: do something
                //System.out.print(".");
            } catch (IOException e){
                System.out.println("ERROR: Some random IO error occured!");
                e.printStackTrace();
            }
        }
    }
    
    public static void sender(DatagramSocket socket, AudioRecorder recorder, InetAddress clientIP) {
        byte[] buffer;
        while (VoIPSystem.running){
            try {
                //Initialise AudioPlayer and AudioRecorder objects
                buffer = recorder.getBlock();
                
                //Make a DatagramPacket from it, with client address and port number
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clientIP, VoIPSystem.OTHER_PORT);

                socket.send(packet);

            } catch (IOException e) {
                System.out.println("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
            }
        }
    }
}
