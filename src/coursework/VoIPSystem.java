package coursework;

import java.util.Scanner;

/*
 * TextDuplex.java
 *
 * Created on 15 January 2003, 17:11
 */

/**
 *
 * @author  abj
 */
public class VoIPSystem {
    
    public enum SocketNum {
        SOCKET1, SOCKET2, SOCKET3, SOCKET4
    }
    
    public enum MissingPacketStrategy {
        FILLING, SPLICING, REPEATING, INTERPOLATION
    }
    
    // 1 - 1
    // 2 - INTERLEAVER 4
    // 3 - INTERLEAVER 2
    // 4 - 4
    
    public static SocketNum USE_SOCKET = SocketNum.SOCKET4;
    public static final int INTERLEAVER_SIZE = 2;
    public static boolean running = true; 
    public static final int VOICE_DATA_SIZE = 512;
    public static final int TIMEOUT = 32;
    
    
    // "139.222.5.203"; - ARTJOM
    //  "139.222.6.194"; - PAVEL
    public static final String OTHER_IP = "139.222.6.194";
    //public static final String OTHER_IP = "localhost";
    public static final int THIS_PORT = 55555;
    public static final int OTHER_PORT = 55555;
    
    
    
    public static final int COMPRESSED_PACKET = 128;
    public static final int COMPRESSED_PACKET_NO = -10;
    
    // Socket 4.
    public static final int CRC_BYTE_SIZE = 2;
    public static final int SOCKET4_PACKET_SIZE = 
                    VOICE_DATA_SIZE + (4 * CRC_BYTE_SIZE);
    public static final int SOCKET4_PACKET_QUARTER_SIZE = SOCKET4_PACKET_SIZE / 4;
    public static final int VOICE_DATA_QUARTER = VOICE_DATA_SIZE / 4;
    
    public static final MissingPacketStrategy SOLUTION4_MISSING_PACKET_STRATEGY = MissingPacketStrategy.INTERPOLATION;
    
    
    public static void main (String[] args) {
        VoiceReceiver receiver = new VoiceReceiver();
        VoiceSender sender = new VoiceSender();
        
        Thread receiverThread = new Thread(receiver);
        receiverThread.start();
        
        Thread senderThread = new Thread(sender);
        senderThread.start();
        
        System.out.println("Type 'exit' to exit.");
        Scanner scanner = new Scanner(System.in);
        String input;
        while (VoIPSystem.running) {
            input = scanner.nextLine();
            if (input.equalsIgnoreCase("exit")) {
                VoIPSystem.running = false;
            }
        }
        
        try {
            senderThread.join();
            receiverThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
