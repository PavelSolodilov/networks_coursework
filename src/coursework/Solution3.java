/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import CMPC3M06.AudioPlayer;
import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author djs15tau
 */
public class Solution3 {
    
    private static int n = VoIPSystem.INTERLEAVER_SIZE;
    private static DatagramPacket[] receiverArray = new DatagramPacket[n * n];
    private static DatagramPacket tempPacket = null;
    private static int interleaverCounter = 1;

    private static int interleaverBufferMaxSize = n*n*2;
    private static PriorityQueue<DatagramPacket> interleaverBuffer = new PriorityQueue<>(new cmpBySeqNo());

    public static class cmpBySeqNo implements Comparator<DatagramPacket> {

        @Override
        public int compare(DatagramPacket p1, DatagramPacket p2) {
            int num1 = getSeqNo2(p1);
            int num2 = getSeqNo2(p2);
            
            return num1 - num2;
        }
    
    }
    
    public static int bufferCounter = 0;
    public static void receiver(DatagramSocket socket, AudioPlayer player, ArrayList<DatagramPacket> playerList, Object playerListLock) {
        while (VoIPSystem.running) {
            byte[] buffer = null;
            
            DatagramPacket toPlay;
            try {
                // Receive a DatagramPacket with voice data of 512 bytes each.
                buffer = new byte[VoIPSystem.VOICE_DATA_SIZE + 4 + VoIPSystem.COMPRESSED_PACKET];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

                socket.receive(packet);
                interleaverBuffer.offer(packet);
                
                int num = getSeqNo2(packet); 
                int buffNum;
                // If the buffer is not full, save the packet into buffer.
                if (interleaverBuffer.size() >= interleaverBufferMaxSize) {
                    // Go through n packets in the buffer.
                    for(int i = 0; i < n*n; i++){
                        
                        // Get the ith packet and calculate it's sequence number.
                        packet = interleaverBuffer.poll();
                        buffNum = getSeqNo2(packet);
                        //System.out.println("GO: " + buffNum);
                        // If in this buffer, save to receiver
                        if (buffNum / (n*n) == bufferCounter) {
                            //System.out.println("YES: " + buffNum + "(" + bufferCounter + ")");
                            //System.out.println(num);
                            // Get the sequence number byte array.
                            receiverArray[buffNum % (n * n)] = packet;
                            tempPacket = packet;
                        } else if (buffNum / (n*n) > bufferCounter){
                            //System.out.println("NO: " + buffNum + "(" + bufferCounter + ")");
                            // If outside, return the packet to the buffer and 
                            // leave the loop.
                            interleaverBuffer.offer(packet);
                            break;
                        } else {
                            break;
                        }
                    }
                    
                    // This buffer has been sent, go for next one.
                    bufferCounter++;
                    
                    
                    toPlay = tempPacket;
                    // Send the player data to the player.
                    for (int i = 0; i < receiverArray.length; i++) {
                        
                        for (int j = i; j < receiverArray.length; j++) {
                            if (receiverArray[j] != null) {
                                toPlay = receiverArray[j];
                            }
                        }
                        
                        if (receiverArray[i] == null) {
                            byte[] seqNo = ByteBuffer.allocate(4).putInt(VoIPSystem.COMPRESSED_PACKET_NO).array();
                            byte[] tempBuffer = new byte[seqNo.length + VoIPSystem.VOICE_DATA_SIZE];

                            // Assign the sequence number.
                            for (int j = 0; j < seqNo.length; j++){
                                tempBuffer[j] = seqNo[j];
                            }

                            int offset = VoIPSystem.VOICE_DATA_SIZE + 4;
                            byte byte1;
                            byte byte2;
                            
                            if (toPlay.getData().length > VoIPSystem.VOICE_DATA_SIZE + 4) {
                                for(int j = 0; j < VoIPSystem.COMPRESSED_PACKET; j += 2) {
                                    byte1 = toPlay.getData()[offset + j];
                                    byte2 = toPlay.getData()[offset + j + 1];

                                    tempBuffer[4 + (j * 4)]     = byte1;
                                    tempBuffer[4 + (j * 4) + 1] = byte2;

                                    tempBuffer[4 + (j * 4) + 2] = byte1;
                                    tempBuffer[4 + (j * 4) + 3] = byte2;

                                    tempBuffer[4 + (j * 4) + 4] = byte1;;
                                    tempBuffer[4 + (j * 4) + 5] = byte2;

                                    tempBuffer[4 + (j * 4) + 6] = byte1;
                                    tempBuffer[4 + (j * 4) + 7] = byte2;
                                }
                            } else {
                                tempBuffer = toPlay.getData();
                            }
                            
                            receiverArray[i] = new DatagramPacket(tempBuffer, 0, tempBuffer.length);
                        }

                        //System.out.println(Arrays.toString(receiverArray[i].getData()));
                        //playerArray[i] = receiverArray[i];
                        synchronized (playerListLock) {
                            playerList.add(receiverArray[i]);
                        }
                    }
                    
                    // Reset the receiver array.
                    receiverArray = new DatagramPacket[n * n];
                    //receiverArray[num % (n * n)] = packet; 
                }
            } catch (SocketTimeoutException e) {
                // No packet received: do something
                //System.out.println(".");
            } catch (IOException e) {
                System.out.println("ERROR: Some random IO error occured!");
                e.printStackTrace();
            }
        }
    }
    
    public static int getSeqNo2(DatagramPacket packet) {
        byte[] seq = new byte[4];
        if (packet == null) {
            return -1;
        }

        for (int i = 0; i < seq.length; i++) {
            seq[i] = packet.getData()[i];
        }

        ByteBuffer wrapped = ByteBuffer.wrap(seq);
        return wrapped.getInt();
    }

    public static byte[] getAudio2(DatagramPacket packet) {
        byte[] audio = new byte[512];
        if (packet == null) {
            return audio;
        }

        for (int i = 0; i < audio.length; i++) {
            audio[i] = packet.getData()[i + 4];
        }
        return audio;
    }
    
    
    private static int sequenceNumber = 0;
    
    //interleaver
    private static ArrayList<DatagramPacket> fillingArray = new ArrayList<>();
    private static ArrayList<DatagramPacket> sendingArray = new ArrayList<>();
    
    public static void sender(DatagramSocket socket, AudioRecorder recorder, InetAddress clientIP) {
        
        try {
            byte[] buffer;
            byte[] seq;
            byte[] payload;
            byte[] prevPacket = new byte[VoIPSystem.COMPRESSED_PACKET];
            DatagramPacket packet;
            while (VoIPSystem.running) {
                //audio
                buffer = recorder.getBlock();

                //seq no
                seq = ByteBuffer.allocate(4).putInt(sequenceNumber++).array();

                //put them together
                payload = new byte[buffer.length + seq.length + VoIPSystem.COMPRESSED_PACKET];
                for (int i = 0; i < seq.length; i++) {
                    payload[i] = seq[i];
                }

                for (int i = 0; i < buffer.length; i++) {
                    payload[i + seq.length] = buffer[i];
                }
                
                for(int i = 0; i < prevPacket.length; i++){
                    payload[i + seq.length + buffer.length] = prevPacket[i];
                }
                
                int compCounter = 0;
                for(int i = 0; i < prevPacket.length; i += 2){
                    prevPacket[i] = buffer[compCounter];
                    prevPacket[i + 1] = buffer[compCounter + 1];
                    compCounter += 4;
                }

                //Make a DatagramPacket from it, with client address and port number
                packet = new DatagramPacket(payload, payload.length, clientIP, VoIPSystem.OTHER_PORT);

                /////////interleaver////////////////////////////////////////////

                //if the array is not filled, fill the array
                if(fillingArray.size() < (n*n)){
                    fillingArray.add(packet);
                }

                //if the array is filled, flip it and send
                if(fillingArray.size() == (n*n)){
                    int counter = 0;
                    for(int i = (n-1); i < fillingArray.size();){
                        sendingArray.add(fillingArray.get(i));

                        i += n;
                        counter++;
                        if(counter < fillingArray.size() && i >= fillingArray.size()){
                            i = i % (fillingArray.size() + 1);
                        }
                    }

                    //send the flipped interleaver
                    for(DatagramPacket p : sendingArray){
                        socket.send(p);
                    }

                    sendingArray.clear();
                    fillingArray.clear();
                }

                /////////interleaver////////////////////////////////////////////
            }
        } catch (IOException e) {
            System.out.println("ERROR: TextSender: Some random IO error occured!");
            e.printStackTrace();
        }
    }
    
}
