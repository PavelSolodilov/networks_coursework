/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import CMPC3M06.AudioPlayer;
import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author cylowatt
 */
public class Solution4 {
    private static Solution4 instance = null;
    
    /**
     * Implementation of a singleton pattern.
     * 
     * @return Instance of the 4th solution to the 4th socket.
     */
    public static Solution4 getInstance() {
        if (Solution4.instance == null) {
            Solution4.instance = new Solution4();
        }
        return Solution4.instance;
    }
    
    
    /**
     * 
     * @param socket
     * @param player
     * @param playerList
     * @param playerListLock 
     */
    public void receiver(DatagramSocket socket, AudioPlayer player, 
            ArrayList<DatagramPacket> playerList, Object playerListLock) {
        try {
            byte[] buffer;
            while (VoIPSystem.running) {
                
                // Construct a buffer packet and receive a UDP packet.
                buffer = new byte[VoIPSystem.SOCKET4_PACKET_SIZE];
                DatagramPacket packet = new DatagramPacket(buffer, 0, 
                        VoIPSystem.SOCKET4_PACKET_SIZE);

                socket.receive(packet);
                
                // Get and play the voice data.
                player.playBlock(this.getVoiceData(packet.getData()));
                
            }
        } catch (Exception e) {
            System.out.println("Failed to play a block: " + e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    
    private enum CorruptQuarter {
        FIRST, SECOND, THIRD, FOURTH, NONE
    }
    
    private byte[][] previousVoice;
    private CorruptQuarter previousCorrupt;
    
    /**
     * 
     * @param packetData
     * @return 
     */
    private byte[] getVoiceData(byte[] packetData) {
        
        int voiceQuarterSize = VoIPSystem.VOICE_DATA_SIZE / 4;
        
        // Retrieve all of the quarters with voice only.
        byte[][] voiceQuarters = extractQuarters(packetData, true);
        int quarterIndex = -1;
        int quarterFrom = -1;
        
        CorruptQuarter corrupt = this.checkQuarters(packetData, voiceQuarters);
        // Detect if any of the quarters have been corrupted.
        switch (corrupt) {
            case FIRST:
                // Copy from the second quarter.
                quarterIndex = 0;
                quarterFrom = 1;
                break;
            case SECOND:
                 // Copy from the third quarter.
                quarterIndex = 1;
                quarterFrom = 2;
                break;
            case THIRD:
                // Copy from the fourth quarter.
                quarterIndex = 2;
                quarterFrom = 3;
                break;
            case FOURTH:
                // Copy from the third quarter.
                quarterIndex = 3;
                quarterFrom = 2;
                break;
        }
        
        // If found an error, resolve.
        if (quarterIndex >= 0 && quarterFrom >= 0) {
            switch (VoIPSystem.SOLUTION4_MISSING_PACKET_STRATEGY) {
                case REPEATING:
                    System.arraycopy(
                        Init.adjustAudio(voiceQuarters[quarterFrom], 0.7), 
                        0, 
                        voiceQuarters[quarterIndex], 
                        0, 
                        voiceQuarters[quarterFrom].length
                    );
                    break;
                case INTERPOLATION:
                    
                    // Need to interpolate previous packet's 4th quarter and this one's 2nd quarter.
                    if (quarterIndex == 0 && previousVoice != null) {
                        voiceQuarters[quarterIndex] = Init.interpolate(voiceQuarters[1], previousVoice[3]);
                        Init.adjustAudio(voiceQuarters[quarterIndex], 0.7);
                        break;
                    } else if (quarterIndex == 3) {
                        voiceQuarters[quarterIndex] = Init.interpolate(voiceQuarters[quarterIndex-1], new byte[voiceQuarterSize]);
                        Init.adjustAudio(voiceQuarters[quarterIndex], 0.7);
//                        System.arraycopy(
//                            Init.adjustAudio(voiceQuarters[quarterFrom], 0.85), 
//                            0, 
//                            voiceQuarters[quarterIndex], 
//                            0, 
//                            voiceQuarters[quarterFrom].length
//                        );
                        break;
                    } else if (quarterIndex == 0 && previousVoice == null) {
                        System.arraycopy(
                            Init.adjustAudio(voiceQuarters[quarterFrom], 0.7), 
                            0, 
                            voiceQuarters[quarterIndex], 
                            0, 
                            voiceQuarters[quarterFrom].length
                        );
                        break;
                    }
                    
                    voiceQuarters[quarterIndex] = Init.interpolate(voiceQuarters[quarterIndex-1], voiceQuarters[quarterIndex+1]);
                    //Init.adjustAudio(voiceQuarters[quarterIndex], 0.7);
                    
                    break;
                default:
                    // Set volume to 0.
                    Init.adjustAudio(voiceQuarters[quarterIndex], 0);
            }
        }
        
        // Reconstruct the voice data from quarters.
        int quarterVoiceSize = VoIPSystem.VOICE_DATA_SIZE / 4;
        byte[] voiceData = new byte[VoIPSystem.VOICE_DATA_SIZE];
        for (int i = 0; i < voiceQuarters.length; i++) {
            // Fill the voice data with each quarter's data:
            /// copy from the quarter at position 0
            /// into the voice data starting at position 128 * i,
            /// and the whole length of the quarter.
            System.arraycopy(voiceQuarters[i], 0, voiceData, quarterVoiceSize * i, 
                    quarterVoiceSize);
        }

        this.previousVoice = voiceQuarters;
        this.previousCorrupt = corrupt;
        
        return voiceData;
    }
    
    
    
    
    /**
     * Extracts quarters from a given byte array.
     * 
     * @param packetData Packet data to extract quarters from.
     * @param voiceQuarter True to only return the voice quarter. False to get
     *                     the whole packet data quarter.
     * @return 
     */
    private byte[][] extractQuarters(byte[] packetData, boolean voiceQuarter) {
        
        // Set the size of the quarter: whether to cut the whole quarter out, or
        // just the voice data.
        int quarterSize;
        if (voiceQuarter) {
            quarterSize = VoIPSystem.VOICE_DATA_QUARTER;
        } else {
            quarterSize = VoIPSystem.SOCKET4_PACKET_QUARTER_SIZE;
        }
        
        // Initialise an array with quarters to return.
        byte[][] quarters = new byte[4][quarterSize];
        
        // Fill the quarters in.
        for (int i = 0; i < quarters.length; i++) {
            /// Copy from packet data 
            // into the current quarter from position 0,
            // and copy however much data is needed.
            System.arraycopy(
                packetData,  // Copy from packet data
                // starting at the quarters' size with CRC (130) multiplied by i
                VoIPSystem.SOCKET4_PACKET_QUARTER_SIZE * i, 
                quarters[i], // into the current quarter
                0,           // starting from position 0
                quarterSize  // all the way up to the requested size
            );
        }
        
        return quarters;
    }
    
    /**
     * Checks whether the quarters have been corrupt.
     * 
     * @param packetData The whole packet.
     * @param voiceQuarters Array of 4 quarters of voice only.
     * @return Which quarter has been corrupted, or NONE if all is OK.
     */
    private CorruptQuarter checkQuarters(byte[] packetData, byte[][] voiceQuarters) {
        
        // Extract the full quarters.
        byte[][] fullQuarters = this.extractQuarters(packetData, false);
       
        byte[] calculatedCrc;
        byte[] realCrc;
        
        // Check if any of the quarters are corrupt.
        for (int i = 0; i < fullQuarters.length; i++) {
            // Calculate the CRC from the received voice quarter.
            calculatedCrc = this.crc16(voiceQuarters[i]);
            
            // Extract the CRC from the received full quarter.
            realCrc = extractCrcFromQuarter(fullQuarters[i]);
            
            // If the CRCs do not match, there is an error.
            if (!Arrays.equals(calculatedCrc, realCrc)) {
                switch (i) {
                    case 0: return CorruptQuarter.FIRST;
                    case 1: return CorruptQuarter.SECOND;
                    case 2: return CorruptQuarter.THIRD;
                    case 3: return CorruptQuarter.FOURTH;
                }
            }
        } // end for
        
        return CorruptQuarter.NONE;
    }
    
    /**
     * Extracts the CRC from quarter as an array of two bytes.
     * 
     * @param quarter Quarter with the CRC to extract from.
     * @return CRC as an array of two bytes.
     */
    private byte[] extractCrcFromQuarter(byte[] quarter) {
        // Create an array to hold the CRC.
        byte[] crc = new byte[VoIPSystem.CRC_BYTE_SIZE];
        
        // The two bytes of the CRC come right after the voice data.
        crc[0] = quarter[VoIPSystem.VOICE_DATA_SIZE / 4];
        crc[1] = quarter[VoIPSystem.VOICE_DATA_SIZE / 4 + 1];
        
        // Return the result.
        return crc;
    }
    
    
    
    /**
     * 
     * @param socket
     * @param recorder
     * @param clientIP 
     */
    public void sender(DatagramSocket socket, AudioRecorder recorder, 
            InetAddress clientIP) {
        
        DatagramPacket packet;
        byte[] voiceData;
        byte[] packetData;
        try {
            while (VoIPSystem.running) {

                // Record the voice data.
                voiceData = recorder.getBlock();

                // Create a packet with CRC.
                packetData = this.crcPacket(voiceData);
                
                // Create the UDP packet to send.
                packet = new DatagramPacket(packetData, packetData.length, 
                        clientIP, VoIPSystem.OTHER_PORT);

                // Send the packet.
                socket.send(packet);
                
            } // end while
        } catch (IOException e) {
            System.out.println("Failed to record a block: " + e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    
    /**
     * Creates a full packet to send with the CRCs from voice data.
     * 
     * @param voiceData Voice data to send over.
     * @return Packet data that can be sent over.
     */
    private byte[] crcPacket(byte[] voiceData) {
        int voiceQSize = VoIPSystem.VOICE_DATA_QUARTER;
        int qSize = VoIPSystem.SOCKET4_PACKET_QUARTER_SIZE;
        
        // Create and fill the packet.
        byte[][] quarters = new byte[4][qSize];
        
        byte[] packet           = new byte[VoIPSystem.SOCKET4_PACKET_SIZE]; 
        byte[] voiceOnlyQuarter = new byte[VoIPSystem.VOICE_DATA_QUARTER];
        
        for (int i = 0; i < quarters.length; i++) {
            
            // Copy the voice data into quarters.
            System.arraycopy(
                    voiceData,      // Copy from the voice data 
                    voiceQSize * i, // at position 128 * i
                    quarters[i],    // into the current quarter at position i
                    0,              // from the start
                    voiceQSize      // until all of the voice data is copied.
            );
            
            // Copy the voice data into additional quarters for CRC calculation.
            System.arraycopy( 
                    quarters[i],        // Copy from the quarter with voice data
                    0,                  // starting from position 0
                    voiceOnlyQuarter,   // into a voice-only quarter
                    0,                  // starting at position 0
                    voiceOnlyQuarter.length // filling it completely.
            );
            
            // Calculate and add CRC's to each of the quarters.
            System.arraycopy(
                    this.crc16(voiceOnlyQuarter), // Copy from calculated CRC
                    0,                            // from the first element
                    quarters[i],                  // into the full quarter
                    voiceQSize,                   // starting from where voice 
                                                  // ends (pos 128)
                    VoIPSystem.CRC_BYTE_SIZE      // amount of CRC bytes.
            );
            
            // Fill the packet with each quarter's data.
            System.arraycopy(
                    quarters[i], // Copy from the quarter at position 0
                    0,           // starting from index 0
                    packet,      // into the resulting packet
                    qSize * i,   // starting at position of the required quarter
                    qSize        // and use the whole quarter's size
            );
        }

        return packet;
    }
    
    
    /**
     * Calculates a CRC-16 for the given byte array.
     * Based on: http://introcs.cs.princeton.edu/java/61data/CRC16CCITT.java
     * 
     * @param data Data to calculate CRC-16 of.
     * @return CRC-16 as a 2-byte array.
     */
    private byte[] crc16(byte[] data) {
        
        int crcValue = 0xFFFF;
        int polynomial = 0x1021;
        
        boolean bit;
        boolean c15;
        for (byte b : data) {
            for (int i = 0; i < 8; i++) {
                bit = ((b   >> (7-i) & 1) == 1);
                c15 = ((crcValue >> 15    & 1) == 1);
                crcValue <<= 1;
                if (c15 ^ bit) crcValue ^= polynomial;
            }
        }
        
        crcValue &= 0xffff;
        
        return Arrays.copyOfRange(
                ByteBuffer.allocate(4).putInt(crcValue).array(), 2, 4
        );
    }
}
