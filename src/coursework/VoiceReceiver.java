package coursework;

import java.net.*;
import java.io.*;

import CMPC3M06.AudioPlayer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

/**
 * VoiceReceiver.java
 *
 * @author abj Modified by Pavel Solodilov on 3rd February 2017.
 */
public class VoiceReceiver implements Runnable {
    private DatagramSocket socket;
    private AudioPlayer player;
    private ArrayList<DatagramPacket> playList;
    private final Object PLAY_LIST_LOCK = new Object();
    /**
     * Runs the receiver.
     */
    public void run() {
        
        // Set up the receiving socket and its timeout.
        this.socket = Init.receiverSocket();

        // Set up the audio player.
        this.player = Init.makeAudioPlayer();

        this.playList = new ArrayList<>();
        
        // Thread to play the received audio packets.

        
        // Run the main loop.
        switch (VoIPSystem.USE_SOCKET) {
            case SOCKET1:
                Solution1.receiver(player, socket);
                break;
            case SOCKET2:
                new ThreadedPlayer().activate();
                Solution2.receiver(socket, player, playList, PLAY_LIST_LOCK);
                break;
            case SOCKET3:
                new ThreadedPlayer().activate();
                Solution3.receiver(socket, player, playList, PLAY_LIST_LOCK);
                break;
            case SOCKET4:
                Solution4.getInstance().receiver(socket, player, playList, PLAY_LIST_LOCK);
                break;
        }

        // Close the socket and the player.
        this.player.close();
        this.socket.close();
    }
    
    private class ThreadedPlayer {
        private ArrayList<DatagramPacket> ownPlayerList = new ArrayList<>();
        public void activate() {
            new Thread(new Runnable(){
                @Override
                public void run() {
                    DatagramPacket tempPacket = null;
                    while (VoIPSystem.running){
                        
                        synchronized (PLAY_LIST_LOCK) {
                            ownPlayerList.addAll(playList);
                            playList.clear();
                        }
                        
                        //interleaver player
                        if (!ownPlayerList.isEmpty()) {
                            System.out.println(Solution2.getSeqNo2(ownPlayerList.get(0)));
                            //if packet is ok, save it and play
                            //if not, then played the saved one
                            if(Solution2.getSeqNo2(ownPlayerList.get(0)) != -1){
                                tempPacket = ownPlayerList.remove(0);
                                try {
                                    player.playBlock(Solution2.getAudio2(tempPacket));
                                } catch (IOException ex) {
                                    Logger.getLogger(VoiceReceiver.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else {
                                ownPlayerList.remove(0);
                                try {
                                   player.playBlock(Solution2.getAudio2(tempPacket));
                                } catch (IOException ex) {
                                    Logger.getLogger(VoiceReceiver.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    }
                }
            }).start();
        }
    }
}
