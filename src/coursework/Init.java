/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import CMPC3M06.AudioPlayer;
import CMPC3M06.AudioRecorder;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import oracle.jrockit.jfr.events.Bits;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

/**
 *
 * @author djs15tau
 */
public class Init {

    public static DatagramSocket receiverSocket() {
        DatagramSocket socket = null;
        
        try {
            switch (VoIPSystem.USE_SOCKET) {
                case SOCKET1:
                    socket = new DatagramSocket(VoIPSystem.THIS_PORT);
                    break;
                case SOCKET2:
                    socket = new DatagramSocket2(VoIPSystem.THIS_PORT);
                    break;
                case SOCKET3:
                    socket = new DatagramSocket3(VoIPSystem.THIS_PORT);
                    break;
                case SOCKET4:
                    socket = new DatagramSocket4(VoIPSystem.THIS_PORT);
                    break;
            }
        } catch (SocketException e) {
            System.out.println("Could not open receiver " + VoIPSystem.USE_SOCKET);
            e.printStackTrace();
            System.exit(1);
        }
        
        return socket;
    }
    
    public static DatagramSocket senderSocket() {
        DatagramSocket socket = null;
        
        try {
            switch (VoIPSystem.USE_SOCKET) {
                case SOCKET1:
                    socket = new DatagramSocket();
                    break;
                case SOCKET2:
                    socket = new DatagramSocket2();
                    break;
                case SOCKET3:
                    socket = new DatagramSocket3();
                    break;
                case SOCKET4:
                    socket = new DatagramSocket4();
                    break;
            }
        } catch (SocketException e) {
            System.out.println("Could not open sender " + VoIPSystem.USE_SOCKET);
            e.printStackTrace();
            System.exit(1);
        }
        
        return socket;
    }
    
    public static AudioPlayer makeAudioPlayer() {
        try {
            return new AudioPlayer();
        } catch (Exception e) {
            System.out.println("Failed to instantiate the audio player.");
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }
    
    public static InetAddress getIp() {
	try {
            return InetAddress.getByName(VoIPSystem.OTHER_IP);
	} catch (UnknownHostException e) {
            System.out.println("ERROR: Could not find client IP.");
            e.printStackTrace();
            System.exit(1);
            return null;
	}
    }
    
    public static AudioRecorder makeAudioRecorder() {
        try {
            return new AudioRecorder();
        } catch (Exception e) {
            System.out.println("Failed to instantiate the audio recorder.");
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }
    
    /**
     * Adjusts audio of the given sound data byte array. Assumes that a single
     * sound frame is 16-bits or 2 bytes long.
     * 
     * @param soundData Sound data to change the volume of. NB this will be
     *                  modified, not copied.
     * @param volume Required volume level. E.g. 0.5 is 50% of the volume, and
     *               0.8 is 80% (20% decrease). Beware of overflow if over 1.0.
     * @return The same sound data, but modified.
     */
    public static byte[] adjustAudio(byte[] soundData, double volume) {
  
        short soundFrame;
        
        // Go through each second index of the sound data.
        for (int i = 0; i < soundData.length ; i += 2) {
            
            // Get the sound frame from the current and next bytes of the data.
            soundFrame = (short) ((soundData[i+1] << 8) | (soundData[i] & 0xFF));
            
            // Change the volume of the frame.
            soundFrame *= volume;
            
            // Little endian.
            soundData[i] = (byte)(soundFrame & 0xFF);
            soundData[i+1] = (byte)((soundFrame >> 8) & 0xFF);
        }
        
        return soundData;
    }
    
    /**
     * Interpolates two byte arrays into a representation of something that
     * sounds roughly between them.
     * Note that the resulting array will have the same length as input ones.
     * 
     * @param first First byte array to mix. Make sure that length matches the 
     *              second one.
     * @param second Second byte array to mix.
     * @return A mix of two byte arrays to create something that sounds roughly
     *         like them both.
     */
    public static byte[] interpolate(byte[] first, byte[] second) {
        byte[] interpolated = new byte[first.length];
        int amp1;
        int amp2;
        int container;
        short soundFrame;
        
        /*
        for (int i = 0; i < first.length; i += 2) {
            amp1 = ((first[i+1] << 8) | (first[i] & 0xFF));
            amp2 = ((second[i+1] << 8) | (second[i] & 0xFF));

            container = amp1 + amp2;
            soundFrame = (short) (container / 2);
            
            interpolated[i] = (byte)(soundFrame & 0xFF);
            interpolated[i+1] = (byte)((soundFrame >> 8) & 0xFF);
        }*/
        
        int firstLast;
        int secondFirst;
        
        firstLast = (first[first.length-1] << 8) | (first[first.length-2] & 0xFF);
        secondFirst = (second[1] << 8) | (second[0] & 0xFF);
        
        int firstMin = Integer.MAX_VALUE;
        int firstMax = Integer.MIN_VALUE;
        int secondMin = Integer.MAX_VALUE;
        int secondMax = Integer.MIN_VALUE;
        
        int firstMean = 0;
        int secondMean = 0;
        
        for (int i = 0; i < first.length; i += 2) {
            amp1 = ((first[i+1] << 8) | (first[i] & 0xFF));
            amp2 = ((second[i+1] << 8) | (second[i] & 0xFF));
            
            if (amp1 < firstMin) {
                firstMin = amp1;
            }
            
            if (amp1 > firstMax) {
                firstMax = amp2;
            }
            
            if (amp2 < secondMin) {
                secondMin = amp1;
            }
            
            if (amp2 > secondMax) {
                secondMax = amp2;
            }
            
            firstMean += amp1;
            secondMean += amp2;
        }
        
        firstMean /= first.length;
        secondMean /= second.length;
        
        int max = (firstMax + secondMax) / 2;
        int min = (firstMin + secondMin) / 2;
        
        int point1;
        int point2 = secondMean;
        
        int oneThird = first.length / 3;
        
        if (firstLast >= firstMean) {
            point1 = (max + firstMean) / 2;
        } else {
            point1 = (min + firstMean) / 2;
        }
        
        
        int stdFirst = 0;
        int stdSecond = 0;
        for (int i = 0; i < first.length - 1; i++) {
            amp1 = ((first[i+1] << 8) | (first[i] & 0xFF));
            amp2 = ((second[i+1] << 8) | (second[i] & 0xFF));
            
            stdFirst += Math.pow(amp1 - firstMean, 2);
            stdSecond += Math.pow(amp2 - secondMean, 2);
        }
        
        stdFirst /= first.length;
        stdSecond /= first.length;
        
        // Standard deviations.
        stdFirst = (int) Math.sqrt(stdFirst);
        stdSecond = (int) Math.sqrt(stdSecond);
       
        
        boolean sameSign = ((firstLast >= 0) && (secondFirst >= 0)) || ((firstLast < 0) && (secondFirst < 0));
        boolean firstOnlyZero = (firstLast == 0 && secondFirst != 0);
        boolean secondOnlyZero = (firstLast != 0 && secondFirst == 0);
        boolean singleZero = firstOnlyZero || secondOnlyZero;
        
        int midPoint;
        if (!sameSign && !singleZero) {
            midPoint = 0;
        } else {
            int firstAndLastMean = (firstLast + secondFirst) / 2;
            int stdMean = (stdFirst + stdSecond) / 2;
            if (stdMean > 3000) stdMean = 3000;
            
            if (firstOnlyZero && secondFirst > 0 || secondOnlyZero && firstLast > 0) {
                midPoint = firstAndLastMean - stdMean;
                
            } else if (firstOnlyZero && secondFirst < 0 || secondOnlyZero && firstLast < 0) {
                midPoint = firstAndLastMean + stdMean;
                
            } else if (firstLast > 0) {
                // Both positive.
                midPoint = firstAndLastMean - stdMean;
                if (midPoint < 0) midPoint = 0;
            } else {
                // Both negative.
                midPoint = firstAndLastMean + stdMean;
                if (midPoint > 0) midPoint = 0;
            }
        }
        
        // --- EQUATIONS --- //
        
        
        // (x, y)
        // (0, amplitude of first)
        // (1/3 of length, point1)
        // (2/3 of length, mean2)
        ParabolicEquation parabola1 = new ParabolicEquation(0, firstLast, oneThird, point1, oneThird*2, secondMean);
        
        // (x, y)
        // (1/3 of length, point1)
        // (2/3 of length, mean2)
        // (length of first, second's first value)
        ParabolicEquation parabola2 = new ParabolicEquation(oneThird, point1, oneThird*2, point2, first.length-1, secondFirst);
        
        LinearEquation leq = new LinearEquation(0, firstLast, first.length - 1, secondFirst);
        
        
        // Always go to 0.
        ParabolicEquation peq = new ParabolicEquation(
                0, firstLast, 
                first.length / 2, 0,
                first.length - 1, secondFirst
        );
        
        // This is the preferred way.
        ParabolicEquation peqStd = new ParabolicEquation(0, firstLast, first.length / 2, midPoint, first.length - 1, secondFirst);
        //peqStd.printString();
        
        Equation eq = peqStd;
        
        // --- END EQUATIONS --- //
        
        interpolated[0] = first[first.length-2];
        interpolated[0] = first[first.length-1];
        
        // Fill from end of first array to 2/3,
        for (int i = 2; i < oneThird * 2; i += 2) {
            // Get the frame for current i (frame, not byte).
            //soundFrame = (short) parabola1.getY(i);
            soundFrame = (short) eq.getY(i);
            //soundFrame = (short) leq.getY(i);
            
            // Insert the values.
            interpolated[i] = (byte)(soundFrame & 0xFF);
            interpolated[i+1] = (byte)((soundFrame >> 8) & 0xFF);
            
            //System.out.println(total + " " + soundFrame);
        }
        
        // 2. Fill the last third.
        for (int i = oneThird*2; i < first.length; i += 2) {
            // Get the frame for current i (frame, not byte).
            //soundFrame = (short) parabola2.getY(i);
            soundFrame = (short) eq.getY(i);
            //soundFrame = (short) leq.getY(i);
            
            // Insert the values.
            interpolated[i] = (byte)(soundFrame & 0xFF);
            interpolated[i+1] = (byte)((soundFrame >> 8) & 0xFF);
            
            //System.out.println(total + " " + soundFrame);
        }
        
        total++;
        
        return interpolated;
    }
    
    private static int total = 0;
    
    public static interface Equation {
        public int getY(int x);
    }
    
    private static class LinearEquation implements Equation {
        private int m;
        private int b;
        
        public LinearEquation(int x1, int y1, int x2, int y2) {
            this.m = (y2-y1)/(x2-x1);
            this.b = y1 - m * x1;
        }
        
        @Override
        public int getY(int x) {
            return this.m * x + b;
        }
    }
    
    /**
     * http://stackoverflow.com/a/717833
     * http://stackoverflow.com/a/16896810
     */
    private static class ParabolicEquation implements Equation {
        private double a;
        private double b;
        private double c;
        
        private int x1;
        private int x2;
        private int x3;
        private int y1;
        private int y2;
        private int y3;
        
        public ParabolicEquation(int x1, int y1, int x2, int y2, int x3, int y3) {
            
            //http://www.vb-helper.com/howto_find_quadratic_curve.html
            this.a = ((y2-y1)*(x1-x3) + (y3-y1)*(x2-x1)) / (double)((x1-x3)*(x2*x2-x1*x1) + (x2-x1)*(x3*x3-x1*x1));
            this.b = ((y2 - y1) - this.a * (x2*x2 - x1*x1)) / (double) (x2-x1);
            this.c = y1 - this.a * x1 * x1 - this.b * x1;
            
//            this.a = y1/((x1-x2)*(x1-x3)) + y2/((x2-x1)*(x2-x3)) + y3/((x3-x1)*(x3-x2));
//            this.b = -y1*(x2+x3)/((x1-x2)*(x1-x3))
//                     -y2*(x1+x3)/((x2-x1)*(x2-x3))
//                     -y3*(x1+x2)/((x3-x1)*(x3-x2));
//            
//            this.c = y1*x2*x3/((x1-x2)*(x1-x3))
//                     + y2*x1*x3/((x2-x1)*(x2-x3))
//                     + y3*x1*x2/((x3-x1)*(x3-x2));
            
            //double denom = (x1 - x2) * (x1 - x3) * (x2 - x3);
            //this.a = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom;
            //this.b = (x3*x3 * (y1 - y2) + x2*x2 * (y3 - y1) + x1*x1 * (y2 - y3)) / denom;
            //this.c = (x2 * x3 * (x2 - x3) * y1 + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom;
            
            this.x1 = x1;
            this.x2 = x2;
            this.x3 = x3;
            this.y1 = y1;
            this.y2 = y2;
            this.y3 = y3;
        }
        
        @Override
        public int getY(int x) {
            return (int) ((this.a * x * x) + (this.b * x) + this.c);
        }
        
        /**
         * Not implemented.
         * @param y
         * @return 
         */
        public int getX(int y) {
            return 0;
        }
        
        public void printString() {
            System.out.printf("y = %5.2fx^2 + %5.2fx + %5.2f\n", a, b, c);
            System.out.printf("(%d, %d) (%d, %d) (%d, %d)\n", x1, y1, x2, y2, x3, y3);
        }
        
    }
}
