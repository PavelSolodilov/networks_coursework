/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursework;

import CMPC3M06.AudioPlayer;
import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author djs15tau
 */
public class Solution2 {
    
    private static int n = VoIPSystem.INTERLEAVER_SIZE;
    private static DatagramPacket[] receiverArray = new DatagramPacket[n * n];
    private static DatagramPacket tempPacket = null;
    private static int interleaverCounter = 1;

    public static void receiver(DatagramSocket socket, AudioPlayer player, ArrayList<DatagramPacket> playerList, Object playerListLock) {
        while (VoIPSystem.running) {
            byte[] buffer = null;
            try {
                // Receive a DatagramPacket with voice data of 512 bytes each.
                buffer = new byte[VoIPSystem.VOICE_DATA_SIZE + 4 + VoIPSystem.COMPRESSED_PACKET];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

                socket.receive(packet);
                /////////////////interleaver////////////////////////////////////

                //if the array is not filled
                int num = getSeqNo2(packet);
                if (num < (n * n * interleaverCounter)) {
                    receiverArray[num % (n * n)] = packet;
                    if(num % (n*n) > 0 && receiverArray[(num % (n * n)) - 1] == null){
                        //
                        // compressedAudio filled with 0 for this packet to make 
                        // all packets the same size 
                        // or nah ?
                        //
                        byte[] seqNo = ByteBuffer.allocate(4).putInt(VoIPSystem.COMPRESSED_PACKET_NO).array();
                        byte[] tempBuffer = new byte[seqNo.length + VoIPSystem.VOICE_DATA_SIZE];
                        
                        //assign -1
                        for(int i = 0; i < seqNo.length; i++){
                            tempBuffer[i] = seqNo[i];
                        }
                        
                        //put compressed packet
                        for(int i = 0; i < VoIPSystem.COMPRESSED_PACKET; i += 2){
                            tempBuffer[4 + (i * 4)] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i];
                            tempBuffer[4 + (i * 4) + 1] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i + 1];
                            
                            tempBuffer[4 + (i * 4) + 2] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i];
                            tempBuffer[4 + (i * 4) + 3] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i + 1];
                            
                            tempBuffer[4 + (i * 4) + 4] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i];
                            tempBuffer[4 + (i * 4) + 5] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i + 1];
                            
                            tempBuffer[4 + (i * 4) + 6] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i];
                            tempBuffer[4 + (i * 4) + 7] = packet.getData()[VoIPSystem.VOICE_DATA_SIZE + 4 + i + 1];
                        }

                        receiverArray[(num % (n * n)) - 1] = new DatagramPacket(tempBuffer, 0, tempBuffer.length);
                        
                    }
                } else {
                    interleaverCounter++;

                    synchronized (playerListLock) {
                        for (int i = 0; i < receiverArray.length; i++) {
                            //playerArray[i] = receiverArray[i];
                            playerList.add(receiverArray[i]);
                        }
                    }
                    receiverArray = new DatagramPacket[n * n];
                    receiverArray[num % (n * n)] = packet;

                }

            } catch (SocketTimeoutException e) {
                // No packet received: do something
                //System.out.println(".");
            } catch (IOException e) {
                System.out.println("ERROR: Some random IO error occured!");
                e.printStackTrace();
            }
        }
    }
    
    public static int getSeqNo2(DatagramPacket packet) {
        byte[] seq = new byte[4];
        if (packet == null) {
            return -1;
        }

        for (int i = 0; i < seq.length; i++) {
            seq[i] = packet.getData()[i];
        }

        ByteBuffer wrapped = ByteBuffer.wrap(seq);
        return wrapped.getInt();
    }

    public static byte[] getAudio2(DatagramPacket packet) {
        byte[] audio = new byte[512];
        if (packet == null) {
            return audio;
        }

        for (int i = 0; i < audio.length; i++) {
            audio[i] = packet.getData()[i + 4];
        }
        return audio;
    }
    
    
    private static int sequenceNumber = 0;
    
    //interleaver
    private static ArrayList<DatagramPacket> fillingArray = new ArrayList<>();
    private static ArrayList<DatagramPacket> sendingArray = new ArrayList<>();
    
    public static void sender(DatagramSocket socket, AudioRecorder recorder, InetAddress clientIP) {
        
        try {
            byte[] buffer;
            byte[] seq;
            byte[] payload;
            byte[] prevPacket = new byte[VoIPSystem.COMPRESSED_PACKET];
            DatagramPacket packet;
            while (VoIPSystem.running) {
                //audio
                buffer = recorder.getBlock();

                //seq no
                seq = ByteBuffer.allocate(4).putInt(sequenceNumber++).array();

                //put them together
                payload = new byte[buffer.length + seq.length + VoIPSystem.COMPRESSED_PACKET];
                for (int i = 0; i < seq.length; i++) {
                    payload[i] = seq[i];
                }

                for (int i = 0; i < buffer.length; i++) {
                    payload[i + seq.length] = buffer[i];
                }
                
                for(int i = 0; i < prevPacket.length; i++){
                    payload[i + seq.length + buffer.length] = prevPacket[i];
                }
                
                int compCounter = 0;
                for(int i = 0; i < prevPacket.length; i += 2){
                    prevPacket[i] = buffer[compCounter];
                    prevPacket[i + 1] = buffer[compCounter + 1];
                    compCounter += 8;
                }

                //Make a DatagramPacket from it, with client address and port number
                packet = new DatagramPacket(payload, payload.length, clientIP, VoIPSystem.OTHER_PORT);

                /////////interleaver////////////////////////////////////////////

                //if the array is not filled, fill the array
                if(fillingArray.size() < (n*n)){
                    fillingArray.add(packet);
                }

                //if the array is filled, flip it and send
                if(fillingArray.size() == (n*n)){
                    int counter = 0;
                    for(int i = (n-1); i < fillingArray.size();){
                        sendingArray.add(fillingArray.get(i));

                        i += n;
                        counter++;
                        if(counter < fillingArray.size() && i >= fillingArray.size()){
                            i = i % (fillingArray.size() + 1);
                        }
                    }

                    //send the flipped interleaver
                    for(DatagramPacket p : sendingArray){
                        socket.send(p);
                    }

                    sendingArray.clear();
                    fillingArray.clear();
                }

                /////////interleaver////////////////////////////////////////////
            }
        } catch (IOException e) {
            System.out.println("ERROR: TextSender: Some random IO error occured!");
            e.printStackTrace();
        }
    }
    
}
