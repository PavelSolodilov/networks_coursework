package coursework;

/*
 * TextSender.java
 *
 * Created on 15 January 2003, 15:29
 */

/**
 *
 * @author  abj
 */
import CMPC3M06.AudioRecorder;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Vector;


public class VoiceSender implements Runnable {
   
    
    private DatagramSocket socket;
    
    public void run (){
        //IP ADDRESS to send to
        InetAddress clientIP = Init.getIp();
        System.out.println("CONN " + clientIP);
        //DatagramSocket sending_socket;
        socket = Init.senderSocket();
        
        // Get a handle to the Standard Input (console) so we can read user input.
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        // Initialise the recorder.
        AudioRecorder recorder = Init.makeAudioRecorder();
        
        //Main loop.
        switch (VoIPSystem.USE_SOCKET) {
            case SOCKET1:
                Solution1.sender(socket, recorder, clientIP);
                break;
            case SOCKET2:
                Solution2.sender(socket, recorder, clientIP);
                break;
            case SOCKET3:
                Solution3.sender(socket, recorder, clientIP);
                break;
            case SOCKET4:
                Solution4.getInstance().sender(socket, recorder, clientIP);
                break;
        }
        
        // Close the socket and the recorder.
        socket.close();
        recorder.close();
    }
    
} 
