package analysis;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * SocketAnalysisPacket.java
 * Used to analyse the sockets' performance. Schema:
 * 4 bytes (int): sequence number.
 * 8 bytes (long): timestamp in millis.
 * 4 bytes (int): hashcode (checksum) of the whole packet with checksum set to 
 *                zero.
 * 512 bytes (byte[]): UDP data.
 * Total: 528 bytes.
 *
 * @author Pavel Solodilov.
 */
public class SocketAnalysisPacket {
    public static final int LENGTH = 528;

    private static final int SEQ_NUMBER_START  = 0;
    private static final int SEQ_NUMBER_LENGTH = 4;

    private static final int TIMESTAMP_START   = 4;
    private static final int TIMESTAMP_LENGTH  = 8;

    private static final int CHECKSUM_START    = 12;
    private static final int CHECKSUM_LENGTH   = 4;

    private static final int DATA_START        = 16;
    private static final int DATA_LENGTH       = VoIPSystem.BUFFER_SIZE;


    private static int sentPackets = 0;

    private int checksum;
    private int sequenceNumber;
    private long sentTimestamp;
    private long receivedTimestamp;
    private byte[] data;

    
    /**
     * Creates a socket analysis packet object out of a full raw byte array:
     * this is a received packet.
     * 
     * @param rawPacket Byte array that conforms to the packet schema.
     * @param timeReceived Timestamp in milliseconds when this packet was 
     *        received.
     * @throws ArrayIndexOutOfBoundsException if the length of the packet is not
     *         exactly {@link#LENGTH}.
     */
    public SocketAnalysisPacket(byte[] rawPacket, long timeReceived) {
        if (rawPacket.length != LENGTH) {
            throw new ArrayIndexOutOfBoundsException("The length of the raw "
                    + "packet needed to be " + SocketAnalysisPacket.LENGTH + 
                    ", provided " + rawPacket.length);
        }

        // Extract the sequence number.
        byte[] sequenceNumberArray = new byte[SEQ_NUMBER_LENGTH];
        System.arraycopy(rawPacket, SEQ_NUMBER_START, sequenceNumberArray, 0, SEQ_NUMBER_LENGTH);
        
        // Extract the sent timestamp.
        byte[] sentTimestampArray = new byte[TIMESTAMP_LENGTH];
        System.arraycopy(rawPacket, TIMESTAMP_START, sentTimestampArray, 0, TIMESTAMP_LENGTH);

        // Extract the checksum.
        byte[] checksumArray = new byte[CHECKSUM_LENGTH];
        System.arraycopy(rawPacket, CHECKSUM_START, checksumArray, 0, CHECKSUM_LENGTH);

        // Extract and set the data.
        this.setData(new byte[DATA_LENGTH]);
        System.arraycopy(rawPacket, DATA_START, this.data, 0, DATA_LENGTH);

        // Set the extracted timestamp, received timestamp and sequence number.
        this.setSentTimestamp(ByteBuffer.wrap(sentTimestampArray).getLong())
            .setSequenceNumber(ByteBuffer.wrap(sequenceNumberArray).getInt())
            .setReceivedTimestamp(timeReceived);
        
        // Set the checksum. Note that this is the received and not the 
        // calculated checksum.
        this.checksum = ByteBuffer.wrap(checksumArray).getInt();
    }
    
    
    /**
     * Constructor for a packet to be sent. The timestamps and sequence numbers 
     * are dealt with automatically in the {@link#makePacket()} method.
     * 
     * @param data {@link#setData(data)}.
     */
    public SocketAnalysisPacket(byte[] data) {
        this.setData(data);
    }

    
    /**
     * Accessor for the sequence number.
     * 
     * @return Sequence number of the packet.
     */
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }
    
    /**
     * Accessor for the timestamp of when the packet was sent.
     * 
     * @return Timestamp in milliseconds: when was this packet sent.
     */
    public long getSentTimestamp() {
        return this.sentTimestamp;
    }
    
    /**
     * Accessor for the timestamp of when the packet was received.
     * 
     * @return Timestamp in milliseconds: when the packet was received. If not
     *         set, will be a negative number.
     */
    public long getReceivedTimestamp() {
        return this.receivedTimestamp;
    }

    /**
     * Accessor for the data.
     * 
     * @return (voice) Data in the packet.
     */
    public byte[] getData() {
        return this.data;
    }
    
    
    /**
     * Mutator for the sequence number.
     * 
     * @param sequenceNumber Sequence number of the packet.
     * @return This object for chaining.
     */
    private SocketAnalysisPacket setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
        return this;
    }
    
    /**
     * Mutator for the timestamp when the packet was sent.
     * 
     * @param timestamp Timestamp in milliseconds.
     * @return This object for chaining.
     */
    private SocketAnalysisPacket setSentTimestamp(long timestamp) {
        this.sentTimestamp = timestamp;
        return this;
    }
        
    /**
     * Mutator for the timestamp when the packet was received.
     * 
     * @param timestamp Timestamp in milliseconds.
     * @return This object for chaining.
     */
    private SocketAnalysisPacket setReceivedTimestamp(long timestamp) {
        this.receivedTimestamp = timestamp;
        return this;
    }
    
    /**
     * Mutator for the data contained in the packet.
     * 
     * @param data New set of data. See schema for size.
     * @return This object for chaining.
     */
    private SocketAnalysisPacket setData(byte[] data) {
        this.data = data;
        return this;
    }
    
    /**
     * Two packets are equal if their set and calculated checksums are equal.
     * 
     * @param other Other packet to compare to.
     * @return True if the packets are equal, and false otherwise.
     */
    @Override
    public boolean equals(Object other) {
        SocketAnalysisPacket otherPacket = (SocketAnalysisPacket) other;
        return (otherPacket.checksum == this.checksum) && 
                (otherPacket.checksum() == this.checksum);
    }
    
    /**
     * Creates a packet that can be sent through a socket.
     * The sent timestamp is recreated in this method, and the amount of sent
     * packets is automatically set and incremented. 
     * <p><b>Do not call this method on a received packet.</b></p>
     * 
     * @return Packet created according to the schema.
     */
    public byte[] makePacket() {
        
        // Set the sent timestamp and the sequence number.
        this.setSentTimestamp(System.currentTimeMillis())
            .setSequenceNumber(SocketAnalysisPacket.sentPackets++);

        return this.bytePacket();
    }

    
    /**
     * Makes a full byte array representing the packet without modifying any of 
     * its current values except for the checksum. Uses the sent timestamp, 
     * sequence number and the data to calculate the checksum and create the
     * packet.
     * 
     * @return Full byte packet array.
     */
    public byte[] bytePacket() {
        
        // Get the new packet.
        byte[] packet = bytePacketNoChecksum();
        
        // Calculate and copy the checksum into the packet.
        this.checksum = Arrays.hashCode(packet);
        System.arraycopy(
                ByteBuffer.allocate(CHECKSUM_LENGTH).putInt(this.checksum).array(),
                0,
                packet,
                CHECKSUM_START,
                CHECKSUM_LENGTH
        );
        
        return packet;
    }
    
    
    /**
     * Creates a byte packet without the checksum. The sent timestamp, sequence 
     * number and the data have to be set in order for this method to return a 
     * correct array.
     * 
     * @return Array representing a packet of bytes.
     */
    public byte[] bytePacketNoChecksum() {
        
        // Allocate the new packet.
        byte[] packet = new byte[SocketAnalysisPacket.LENGTH];
        
        // Copy the data into the end of the packet.
        System.arraycopy(this.data, 0, packet, DATA_START, DATA_LENGTH);
        
        // Copy the sequence number into the start of the packet.
        System.arraycopy(
            ByteBuffer.allocate(SEQ_NUMBER_LENGTH).putInt(this.getSequenceNumber()).array(), 
                0, 
                packet, 
                SEQ_NUMBER_START, 
                SEQ_NUMBER_LENGTH
        );
        
        // Copy the timestamp after the sequence number.
        System.arraycopy(
                ByteBuffer.allocate(TIMESTAMP_LENGTH).putLong(this.getSentTimestamp()).array(),
                0, 
                packet, 
                TIMESTAMP_START, 
                TIMESTAMP_LENGTH
        );
        
        return packet;
    }
    
    
    /**
     * Calculates the checksum. The sent timestamp, sequence number and the data
     * have to be set in order for this method to return a correct value.
     * 
     * @return Checksum of the this packet. 
     */
    public int checksum() {
        return Arrays.hashCode(bytePacketNoChecksum());
    }
    
    
    /**
     * Checks whether the packet has been corrupted. Should only be called when 
     * received and checksum set in the constructor.
     * 
     * @return True if the packet has been corrupted, and false if not.
     */
    public boolean isCorrupted() {
        // Check whether the set checksum equals the calculated checksum.
        return this.checksum != this.checksum();
    }
}
