package analysis;

/*
 * TextSender.java
 *
 * Created on 15 January 2003, 15:29
 */

/**
 *
 * @author  abj
 */
import CMPC3M06.AudioRecorder;
import java.net.*;
import java.io.*;

public class TextSenderThread implements Runnable {
    
    // "139.222.5.203"; - ARTJOM
    //  "139.222.6.194"; - PAVEL
    //private static String ip =  "139.222.5.203";
    private static String ip = "127.0.0.1";
    
    static DatagramSocket sending_socket;

    @Override
    public void run (){
        //***************************************************
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
	try {
            clientIP = InetAddress.getByName(ip);
	} catch (UnknownHostException e) {
            System.out.println("ERROR: TextSender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
	}
        //***************************************************
        
        //***************************************************
        //Open a socket to send from
        //We dont need to know its port number as we never send anything to it.
        //We need the try and catch block to make sure no errors occur.
        
        //DatagramSocket sending_socket;
        try{
            sending_socket = VoIPSystem.newSocket(-1);
	} catch (SocketException e){
            System.out.println("ERROR: TextSender: Could not open UDP socket to send from.");
            e.printStackTrace();
            System.exit(0);
	}
        //***************************************************
      
        //***************************************************
        //Get a handle to the Standard Input (console) so we can read user input
        
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        //***************************************************
        
        //***************************************************
        //Main loop.
        AudioRecorder recorder = null;
        try {
            recorder = new AudioRecorder();
        } catch (Exception e) {
            System.out.println("Failed to instantiate the recorder.");
            e.printStackTrace();
            System.exit(1);
        }
        
        while (VoIPSystem.running){
            try {
                //Initialise AudioPlayer and AudioRecorder objects
                byte[] buffer = recorder.getBlock();
                buffer = new byte[buffer.length];
                for (int i = 0; i < buffer.length; i++) {
                    buffer[i] = 100;
                }
                
                SocketAnalysisPacket sap = new SocketAnalysisPacket(buffer);
                byte[] toSend = sap.makePacket();
                
                //Make a DatagramPacket from it, with client address and port number
                //DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clientIP, PORT);
                DatagramPacket packet = new DatagramPacket(toSend, toSend.length, clientIP, PORT);
            
                //Send it
                SocketAnalyser.getInstance().addSentPacket(sap);
                sending_socket.send(packet);
                //packet.setData(buffer, 1, buffer.length);

            } catch (IOException e) {
                System.out.println("ERROR: TextSender: Some random IO error occured!");
                e.printStackTrace();
            }
        }
        //Close the socket
        sending_socket.close();
        //***************************************************
    }
} 
