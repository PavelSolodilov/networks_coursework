package analysis;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * SocketAnalyser.java
 * Provides a 
 * 
 * @author Pavel Solodilov
 */
public class SocketAnalyser {
    private static SocketAnalyser instance = null;
    
    /**
     * Accessor for a single socket analyser object.
     * 
     * @return Singleton of a socket analyser object.
     */
    public static SocketAnalyser getInstance() {
        if (SocketAnalyser.instance == null) {
            SocketAnalyser.instance = new SocketAnalyser();
        }
        
        return SocketAnalyser.instance;
    }
    
    
    private TreeMap<Integer, SocketAnalysisPacket> sentPackets;
    private TreeMap<Integer, SocketAnalysisPacket> receivedPackets;
    private ArrayList<SocketAnalysisPacket> brokenReceivedPackets;
    int offset = 0;
    
    /**
     * Constructor.
     * Note that it is private in order to conform to the requirements of a
     * singleton pattern.
     */
    private SocketAnalyser() {
        this.sentPackets = new TreeMap<>();
        this.receivedPackets = new TreeMap<>();
        this.brokenReceivedPackets = new ArrayList<>();
    }
    
    
    /**
     * Adds a sent packet into the analyser for future analysis.
     * 
     * @param packet Sent packet to add.
     * @return This object for chaining.
     */
    public synchronized SocketAnalyser addSentPacket(SocketAnalysisPacket packet) {
        //System.out.println("SENT: " + packet.getSequenceNumber());
        this.sentPackets.put(packet.getSequenceNumber(), packet);
        return this;
    }
    
    private boolean printedBroken = false;
    private boolean printedFine= false;
    
    byte[] finePacket = null;
    
    /**
     * Adds a received packet into the analyser for future analysis.
     * 
     * @param packet Received packet to add.
     * @return This object for chaining.
     */
    public synchronized SocketAnalyser addReceivedPacket(
            SocketAnalysisPacket packet) {
        if (packet.isCorrupted()) {
            if (finePacket != null) {
                this.printArraysAndDifference(finePacket, packet.bytePacketNoChecksum(), true);
            }
            //System.out.print("BR: ");
            //printBrokenVoice(packet.bytePacket(), true);
            this.brokenReceivedPackets.add(packet);
        } else {
            if (finePacket == null) {
                finePacket = packet.bytePacket();
//                System.out.print("OK: ");
//                printBrokenVoice(packet.bytePacket(), true);
//                printedFine = true;
            }
            System.out.println("REC: " + packet.getSequenceNumber());
            this.receivedPackets.put(packet.getSequenceNumber(), packet);
        }
        
        return this;
    }
    
    private TreeMap<String, Integer> indicesMap = new TreeMap<>();
    
    private void printArraysAndDifference(byte[] arrayFine, byte[] arrayBroken, boolean bits) {
        /*System.out.print("FINE: ");
        printBrokenVoice(arrayFine, bits);
        
        System.out.print("BRKN: ");
        printBrokenVoice(arrayBroken, bits);
        
        String printString = bits ? "%5d" : "%7d";
        System.out.print("DIFF: ");
        for (int i = 0; i < arrayFine.length; i++) {
            System.out.printf("%5d", arrayFine[i] - arrayBroken[i]);
        }
        System.out.println("");
        
        System.out.print("INDX: ");
        for (int i = 0; i < arrayFine.length; i++) {
            System.out.printf("%5d", i);
        }
        
        System.out.println("");*/
        
        // The header has 16 bytes, or 8 shorts.
        int endHeader = 16;
        
        boolean doBits = false;
        
        short numFine;
        short numBroken;
        
        StringBuilder brokenIndices = new StringBuilder();
        StringBuilder brokenValues = new StringBuilder();
        System.out.print("BROKEN INDICES: ");
        if (doBits) {     
            for (int i = 0; i < arrayFine.length; i++) {
                if (arrayFine[i] - arrayBroken[i] != 0 && i >= endHeader) {
                    brokenIndices.append(i);
                    brokenIndices.append(", ");
                }
            }
        } else {
            for (int i = 0; i < arrayFine.length; i+=2) {
                numFine = arrayFine[i];
                numFine <<= 8;
                numFine |= arrayFine[i+1];
                
                numBroken = arrayBroken[i];
                numBroken <<= 8;
                numBroken |= arrayBroken[i+1];
                
                if (numFine - numBroken != 0 && i >= endHeader) {
                    brokenValues.append(String.format("%5d, ", numFine - numBroken));
                    brokenIndices.append(String.format("%5d, ", i / 2));
//                    brokenIndices.append(i);
//                    brokenIndices.append("-");
//                    brokenIndices.append(i+i);
//                    brokenIndices.append(", ");
                }
            }
        }
        
        String indicesString = brokenIndices.toString();
        String brokenValuesString = brokenValues.toString();
        if (indicesMap.containsKey(indicesString)) {
            indicesMap.replace(indicesString, indicesMap.get(indicesString) + 1);
        } else {
            indicesMap.put(indicesString, 1);
        }
        
        
        System.out.println(indicesString);
        
        //System.out.print("VALUE DIFFS:    ");
        //System.out.println(brokenValuesString);
    }
    
    
    
    
    public static void printBrokenVoice(byte[] array, boolean bits) {
        
        if (bits) {
            for (byte b : array) {
                System.out.printf("%5d", b);
            }
        } else {
            short num;
            for (int i = 0; i < array.length / 2; i+=2) {
                num = array[i];
                num <<= 8;
                num |= array[i+1];
                System.out.printf("%7d", num);
            }
        }
        
        System.out.println("");
    }
    
    
    /**
     * Provides a complete textual analysis of the data currently saved in the
     * analyser. The packet burst loss mean is, if crudely, adjusted for broken 
     * packets.
     * 
     * Packets sent/received:       100/80, ~20% loss
     * Intact/corrupted packets:    78/2, ~2.56% corruption
     * Packet burst loss mean:      3 in a row
     * Lost   Frequency
     * 1      10
     * 2      2
     * 3      7
     * 4      5
     * 6      2
     */
    public void analyse() {
        this.printSentReceivedRatio();
        this.printIntactCorruptedPackets();
        this.printLossStatsAndMean();
        
        for (Map.Entry<String, Integer> e : this.indicesMap.entrySet()) {
            System.out.println(e.getValue() + ": " + e.getKey());
        }
    }
    
    
    /**
     * Prints out the ratio of sent and received packets.
     */
    private void printSentReceivedRatio() {
        int packetsSent = this.sentPackets.size();
        int packetsReceived = 
                this.receivedPackets.size() + this.brokenReceivedPackets.size();
        double ratio = 100 - (((double)packetsReceived/packetsSent) * 100);
        
        System.out.printf("%-28s %d/%d, ~%5.2f%% loss\n", 
            "Packets sent/received:", 
            packetsSent,
            packetsReceived,
            ratio
        );
    }
    
    
    /**
     * Prints out the ration of corrupted and intact packets.
     */
    private void printIntactCorruptedPackets() {
        int packetsIntact = this.receivedPackets.size();
        int packetsCorrupted = this.brokenReceivedPackets.size();
        
        double ratio;
        if (packetsIntact == 0) {
            ratio = 100;
        } else {
            ratio = ((double) packetsCorrupted / packetsIntact) * 100;
        }
        
        
        System.out.printf("%-28s %d/%d, ~%5.2f%% corruption\n",
                "Intact/corrupted packets:",
                packetsIntact,
                packetsCorrupted,
                ratio
        );
    }
    
    
    /**
     * Calculates and prints the lost packet's burst mean and burst statistics.
     */
    private void printLossStatsAndMean() {
        int burstsInTotal = 0;
        int burstLength = 0;
        TreeMap<Integer, Integer> lengthsOfBursts = new TreeMap<>();
        
        SocketAnalysisPacket receivedPacket;
        
        // Calculate the mean packet burst loss by counting the total amount of 
        // bursts (e.g. 10), and then dividing the total amount of packets 
        // lost (e.g. 80) by this amount of bursts. This gives us a mean number
        // of packets lost per burst (80/10 = 8 packets per burst).
        
        // Go through each of the sent packets (none will be missing).
        for (Map.Entry<Integer, SocketAnalysisPacket> entry : this.sentPackets.entrySet()) {
            
            // Try to get the packet with the same sequence number from received
            // packets.
            receivedPacket = this.receivedPackets.get(entry.getKey());
            
            if (receivedPacket != null) {
                
                // If the bursts already contain an entry for this burst, get
                // its total count (value), and increment it by 1.
                /// Otherwise simply add it to the bursts map.
                if (burstLength != 0 && lengthsOfBursts.containsKey(burstLength)) {
                    lengthsOfBursts.replace(burstLength, lengthsOfBursts.get(burstLength) + 1);
                    burstLength = 0;
                } else if (burstLength != 0) {
                    lengthsOfBursts.put(burstLength, 1);
                    burstLength = 0;
                }
                
            } else if (burstLength == 0) {
                // If this is only a start of a burst, count it as such.
                burstsInTotal++;
                burstLength++;
            } else {
                // If it is continuation of a burst, increment the length.
                burstLength++;
            }
        }

        this.printBurstLossMean(burstsInTotal);
        this.printBurstStats(lengthsOfBursts);
    }
    
    
    /**
     * Calculates and prints out the burst loss mean.
     * 
     * @param lossBurstCount Amount of bursts of packet losses.
     */
    private void printBurstLossMean(int lossBurstCount) {
        // Calculate the total amount of packets lost.
        int packetsLost = this.sentPackets.size() - 
                          this.receivedPackets.size() - 
                          this.brokenReceivedPackets.size();
        
        // If no packets were lost, the mean of packets lost is 0.
        /// Otherwise divide the amount of packets lost by loss burst count to
        /// calculate the mean.
        double meanOfPacketsLost;
        if (lossBurstCount == 0 || packetsLost == 0) {
            meanOfPacketsLost = 0;
        } else {
            meanOfPacketsLost = (double) packetsLost / lossBurstCount;
        }
        
        // Adjust for corrupted packets: divide the amount of broken packets by
        // amount of received packets, subract 1, and multiply the resulting
        // value by the packet loss mean in order to decrease it if needed.
        /// Crude.
        if (this.receivedPackets.size() > 0) {
            double corruptedPackets = this.brokenReceivedPackets.size();
            
            meanOfPacketsLost *= 1 - 
                    (corruptedPackets / this.receivedPackets.size());
        }
        
        // Print out the package burst loss mean.
        System.out.printf("%-28s %5.2f in a row\n", "Packet burst loss mean:", 
                meanOfPacketsLost);
    }
    
    
    /**
     * Prints out the burst statistics for the given map of stats.
     * 
     * @param stats The keys are lengths of bursts, and the values are amounts
     *              of how many times they were encountered.
     */
    private void printBurstStats(TreeMap<Integer, Integer> stats) {
        System.out.println("Lost   Frequency");
        
        // Iterate through every stat. They should be in sorted order.
        for (Map.Entry<Integer, Integer> entry : stats.entrySet()) {
            System.out.printf("%-4d   %d\n", entry.getKey(), entry.getValue());
        }
    }
}
