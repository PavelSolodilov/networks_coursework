package analysis;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Scanner;

import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

/*
 * TextDuplex.java
 *
 * Created on 15 January 2003, 17:11
 */

/**
 *
 * @author  abj
 */
public class VoIPSystem {

    
    public static DatagramSocket newSocket(int port) throws SocketException {
        if (port < 1) {
            // Change sockets here for different tests.
            return new DatagramSocket3();
        }
        
        return new DatagramSocket3(port);
    }
    
    public static boolean running = true;
    public static final int BUFFER_SIZE = 512;
    
    /* // UNCOMMENT TO USE
    public static void main (String[] args){
        
        VoiceReceiver receiver = new VoiceReceiver();
        TextSenderThread sender = new TextSenderThread();
        
        Thread receiverThread = new Thread(receiver);
        receiverThread.start();
        
        Thread senderThread = new Thread(sender);
        senderThread.start();
        
        Scanner scanner = new Scanner(System.in);
        String input;
        while (VoIPSystem.running) {
            input = scanner.nextLine();
            if (input.equalsIgnoreCase("exit")) {
                VoIPSystem.running = false;
            }
        }
        
        try {
            senderThread.join();
            receiverThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        SocketAnalyser.getInstance().analyse();
    }*/
    
}
