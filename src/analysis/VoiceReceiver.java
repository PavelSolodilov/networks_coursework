package analysis;

import java.net.*;
import java.io.*;

import CMPC3M06.AudioPlayer;

/**
 * VoiceReceiver.java
 * 
 * @author abj
 * Modified by Pavel Solodilov on 3rd February 2017.
 */
public class VoiceReceiver implements Runnable {
    private static int port = 55555;
    private DatagramSocket receiving_socket;
    private AudioPlayer player;
    private int timeout = 32;
    private Thread thread;
    
    /**
     * Runs the receiver.
     */
    @Override
    public void run (){

        // Set up the receiving socket and its timeout.
        try {
            this.receiving_socket = VoIPSystem.newSocket(port);
            this.receiving_socket.setSoTimeout(timeout);
	} catch (SocketException e) {
            System.out.println("Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
	}
        
        // Set up the audio player.
        try {
            this.player = new AudioPlayer();
        } catch (Exception e) {
            System.out.println("Failed to instantiate the audio player.");
            e.printStackTrace();
            System.exit(1);
        }
        
        // Run the main loop.
        while (VoIPSystem.running){
            byte[] buffer = null;
            
            try {
                // Receive a DatagramPacket with voice data of 512 bits each.
                buffer = new byte[SocketAnalysisPacket.LENGTH];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

                this.receiving_socket.receive(packet);
                
                // TODO receive the first 32 bits of the sequence number.
                
                SocketAnalysisPacket received = new SocketAnalysisPacket(buffer, System.currentTimeMillis());
                SocketAnalyser.getInstance().addReceivedPacket(received);
                
                // The UDP packet contains the voice data. Play it out.
                this.player.playBlock(received.getData());
                
            } catch (SocketTimeoutException e) {
                try {
                    //this.player.playBlock(buffer);
                } catch (Exception ex) {
                    System.out.println("NO PLAY");
                }
                // No packet received: do something
                //System.out.print(".");
            } catch (IOException e){
                System.out.println("ERROR: Some random IO error occured!");
                e.printStackTrace();
            }
        }
        
        // Close the socket and the player.
        this.player.close();
        this.receiving_socket.close();
    }
}
